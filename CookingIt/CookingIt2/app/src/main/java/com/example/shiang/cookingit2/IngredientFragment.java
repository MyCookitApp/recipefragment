package com.example.shiang.cookingit2;


import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

public class IngredientFragment extends Fragment {

    private Button mNewIngredientbtn;
    private Button mDonebtn;
    private Button mCancelbtn;
    private TextView mIngredientsTextView;
    private TextView mDescriptionOfTheIngredientsTextView;

    public IngredientFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View displayView = inflater.inflate(R.layout.fragment_ingredient, container, false);

        mNewIngredientbtn = (Button)displayView.findViewById(R.id.fragment_ingredient_btn_newingredient);
        mDonebtn = (Button)displayView.findViewById(R.id.fragment_ingredient_btn_done);
        mCancelbtn = (Button)displayView.findViewById(R.id.fragment_ingredient_btn_cancel);
        mIngredientsTextView = (TextView) displayView.findViewById(R.id.fragment_ingredient_tv_ingredients);
        mDescriptionOfTheIngredientsTextView = (TextView)displayView.findViewById(R.id.fragment_ingredient_tv_descriptionoftheingredients);

        return displayView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mDonebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().setResult(Activity.RESULT_OK);
                getActivity().finish();
            }
        });

        mCancelbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().setResult(Activity.RESULT_CANCELED);
                getActivity().finish();
            }
        });
    }
}