package com.example.shiang.cookingit2;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;


public class AddNewRecipeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_new_recipe);
    getSupportFragmentManager().beginTransaction()
            .add(R.id.activity_add_new_recipe_view_group, new AddNewRecipeFragment())
            .commit();
    }

}
