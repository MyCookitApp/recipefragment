package com.example.shiang.cookingit2;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class AddNewRecipeFragment extends Fragment {

    private Button mAddPhotobtn;
    private Button mAddIngredientbtn;
    private Button mAddCookingStepsbtn;
    private Button mChooseCategorybtn;
    private Button mSubmitRecipebtn;
    private TextView mAddNewRecipeTextView;
    private EditText mEnterRecipeTitleEditText;
    private EditText mEnterDescriptionEditText;
    private TextView mIngredientsTextView;
    private TextView mCookingStepsTextView;
    private TextView mFoodCategoriesTextview;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View displayView = inflater.inflate(R.layout.fragment_add_new_recipe, container, false);

        mAddPhotobtn = (Button) displayView.findViewById(R.id.fragment_addnewrecipe_btn_addphoto);
        mAddIngredientbtn = (Button) displayView.findViewById(R.id.fragment_addnewrecipe_btn_addingredient);
        mAddCookingStepsbtn = (Button) displayView.findViewById(R.id.fragment_addnewrecipe_btn_addcookingsteps);
        mChooseCategorybtn = (Button) displayView.findViewById(R.id.fragment_addnewrecipe_btn_choosecategory);
        mSubmitRecipebtn = (Button) displayView.findViewById(R.id.fragment_addnewrecipe_btn_submitrecipe);
        mAddNewRecipeTextView = (TextView) displayView.findViewById(R.id.fragment_addnewrecipe_tv_addnewrecipe);
        mEnterRecipeTitleEditText = (EditText) displayView.findViewById(R.id.fragment_addnewrecipe_et_enterrecipetitle);
        mEnterDescriptionEditText = (EditText) displayView.findViewById(R.id.fragment_addnewrecipe_et_enterdescription);
        mIngredientsTextView = (TextView) displayView.findViewById(R.id.fragment_addnewrecipe_tv_ingredients);
        mCookingStepsTextView = (TextView) displayView.findViewById(R.id.fragment_addnewrecipe_tv_cookingsteps);
        mFoodCategoriesTextview = (TextView) displayView.findViewById(R.id.fragment_addnewrecipe_tv_foodcategories);

        return displayView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mAddPhotobtn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().beginTransaction()
                        .replace(R.id.activity_add_new_recipe_view_group, new IngredientFragment())
                        .commit();
            }
        });
        mAddIngredientbtn.setOnClickListener (new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().beginTransaction()
                        .replace(R.id.activity_add_new_recipe_view_group, new IngredientFragment())
                        .commit();
            }
        });
        mAddCookingStepsbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().beginTransaction()
                        .replace(R.id.activity_add_new_recipe_view_group, new IngredientFragment())
                        .commit();
            }
        });
        mChooseCategorybtn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().beginTransaction()
                        .replace(R.id.activity_add_new_recipe_view_group, new IngredientFragment())
                        .commit();
            }
        });
        mSubmitRecipebtn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().beginTransaction()
                        .replace(R.id.activity_add_new_recipe_view_group, new IngredientFragment())
                        .commit();
                }
        });
    }
}
